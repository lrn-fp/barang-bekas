<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class CategoryAndProductSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'name' => 'Electronic',
                'products' => [
                    [
                        'name' => 'Samsung TV 32',
                        'description' => 'Samsung TV 32 inch pembelian februari 2022',
                        'price' => '900000.00',
                        'buy_date' => 1643760000,
                        'warranty_expired_date' => null,
                        'quantity' => 4,
                    ]
                ]
            ],
            [
                'name' => 'Furniture',
                'products' => [
                    [
                        'name' => 'Meja Kopi Jati',
                        'description' => 'Meja Kopi Jati Belanda',
                        'price' => '700000.00',
                        'buy_date' => 1643760000,
                        'warranty_expired_date' => null,
                        'quantity' => 1,
                    ]
                ]
            ],
        ];

        foreach ($data as $categoryData) {
            $category = ['name' => $categoryData['name']];
            $this->db->query('INSERT INTO category (name) VALUES(:name:)', $category);
            $categoryId = $this->db->insertID();
            foreach ($categoryData['products'] as $product) {
                $this->db->query('INSERT INTO product (name, description, price, buy_date, warranty_expired_date, quantity) VALUES(:name:, :description:, :price:, :buy_date:, :warranty_expired_date:, :quantity:)', $product);
                $productId = $this->db->insertID();
                $productLog = $product;
                $productLog['product_id'] = $productId;
                $this->db->query('INSERT INTO product_log (name, description, price, buy_date, warranty_expired_date, quantity) VALUES(:name:, :description:, :price:, :buy_date:, :warranty_expired_date:, :quantity:)', $productLog);
                $productCategory = [
                    'product_id' => $productId,
                    'category_id' => $categoryId,
                ];
                $this->db->query('INSERT INTO product_category (product_id, category_id) VALUES(:product_id:, :category_id:)', $productCategory);
            }
        }
    }
}
