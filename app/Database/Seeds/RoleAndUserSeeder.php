<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class RoleAndUserSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'name' => 'Admin',
                'users' => [
                    [
                        'name' => 'admin',
                        'email' => 'admin@me.com',
                        'password' => password_hash('admin', PASSWORD_BCRYPT),
                    ]
                ]
            ],
            [
                'name' => 'Customer',
                'users' => [
                    [
                        'name' => 'customer',
                        'email' => 'customer@me.com',
                        'password' => password_hash('customer', PASSWORD_BCRYPT),
                    ]
                ]
            ],
        ];

        foreach ($data as $roleData) {
            $role = ['name' => $roleData['name']];
            $this->db->query('INSERT INTO role (name) VALUES(:name:)', $role);
            $roleId = $this->db->insertID();
            foreach ($roleData['users'] as $user) {
                $this->db->query('INSERT INTO user (name, email, password) VALUES(:name:, :email:, :password:)', $user);
                $userId = $this->db->insertID();
                $userRole = [
                    'user_id' => $userId,
                    'role_id' => $roleId,
                ];
                $this->db->query('INSERT INTO user_role (user_id, role_id) VALUES(:user_id:, :role_id:)', $userRole);
            }
        }
    }
}
