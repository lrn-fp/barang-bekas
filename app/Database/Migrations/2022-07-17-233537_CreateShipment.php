<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;
use CodeIgniter\Database\RawSql;

class CreateShipment extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 20,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'shipping_vendor_id' => [
                'type' => 'INT',
                'constraint' => 20,
                'null' => false,
            ],
            'address' => [
                'type' => 'TEXT',
                'null' => false,
            ],
            'geo_lat' => [
                'type' => 'DECIMAL',
                'constraint' => '10,8',
                'null' => true,
            ],
            'geo_lng' => [
                'type' => 'DECIMAL',
                'constraint' => '11,9',
                'null' => true,
            ],
            'recipient' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => false,
            ],
            'additional_info' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => true,
            ],
            'state' => [
                'type' => 'INT',
                'constraint' => 6,
            ],
            'created_at' => [
                'type'    => 'TIMESTAMP',
                'default' => new RawSql('CURRENT_TIMESTAMP'),
            ],
            'updated_at' => [
                'type'    => 'TIMESTAMP',
                'null' => true,
            ],
            'deleted_at' => [
                'type'    => 'TIMESTAMP',
                'null' => true,
            ],
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('shipment');
    }

    public function down()
    {
        $this->forge->dropTable('shipment');
    }
}
