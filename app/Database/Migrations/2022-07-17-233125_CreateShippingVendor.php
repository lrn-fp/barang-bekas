<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;
use CodeIgniter\Database\RawSql;

class CreateShippingVendor extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 20,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => '255',
            ],
            'type' => [
                'type' => 'VARCHAR',
                'constraint' => '20',
                'null' => false,
            ],
            'base_fee' => [
                'type' => 'DECIMAL',
                'constraint' => '19,2',
                'null' => false,
            ],
            'price_type' => [
                'type' => 'VARCHAR',
                'constraint' => '20',
                'null' => false,
            ],
            'price' => [
                'type' => 'DECIMAL',
                'constraint' => '19,2',
                'null' => false,
            ],
            'state' => [
                'type' => 'INT',
                'constraint' => 6,
            ],
            'created_at' => [
                'type'    => 'TIMESTAMP',
                'default' => new RawSql('CURRENT_TIMESTAMP'),
            ],
            'updated_at' => [
                'type'    => 'TIMESTAMP',
                'null' => true,
            ],
            'deleted_at' => [
                'type'    => 'TIMESTAMP',
                'null' => true,
            ],
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('shipping_vendor');
    }

    public function down()
    {
        $this->forge->dropTable('shipping_vendor');
    }
}
