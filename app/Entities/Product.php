<?php

namespace App\Entities;

use CodeIgniter\Entity\Entity;
use CodeIgniter\I18n\Time;

class Product extends Entity
{
  protected $dates = ['buy_date', 'warranty_expired_date', 'created_at', 'updated_at', 'deleted_at'];
}