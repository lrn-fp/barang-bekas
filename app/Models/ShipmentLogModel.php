<?php

namespace App\Models;

use CodeIgniter\Model;

class ShipmentLogModel extends Model
{
    protected $table      = 'shipment_log';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = \App\Entities\ShipmentLog::class;

    protected $allowedFields = [
      'shipment_id',
      'description',
      'state',
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
}