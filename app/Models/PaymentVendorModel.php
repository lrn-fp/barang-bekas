<?php

namespace App\Models;

use CodeIgniter\Model;

class PaymentVendorModel extends Model
{
    protected $table      = 'payment_vendor';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = \App\Entities\PaymentVendor::class;

    protected $allowedFields = [
      'name',
      'type',
      'fee',
      'state',
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
}