<?php

namespace App\Models;

use CodeIgniter\Model;

class ShippingVendorModel extends Model
{
    protected $table      = 'shipping_vendor';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = \App\Entities\ShippingVendor::class;

    protected $allowedFields = [
      'name',
      'type',
      'base_fee',
      'price_type',
      'price',
      'state',
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
}