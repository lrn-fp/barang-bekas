<?php

namespace App\Models;

use CodeIgniter\Model;

class ProductLogModel extends Model
{
    protected $table      = 'product_log';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = \App\Entities\ProductLog::class;

    protected $allowedFields = [
      'product_id',
      'name',
      'description',
      'price',
      'buy_date',
      'warranty_expired_date',
      'state',
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
}