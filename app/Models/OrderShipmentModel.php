<?php

namespace App\Models;

use CodeIgniter\Model;

class OrderShipmentModel extends Model
{
    protected $table      = 'order_shipment';
    protected $primaryKey = 'id';

    protected $useAutoIncrement = true;

    protected $returnType     = \App\Entities\OrderShipment::class;

    protected $allowedFields = [
      'shipment_id',
      'order_id',
      'state',
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
}