
<div class="card" style="margin:20px 0px;">
  <div class="card-img-top">
    <div class="row" style="margin:20px;">
      <div class="col col-12 col-sm-4">
        <div class="w-100">
          <div class="row">
            <div class="col col-12 main-image">
              <?php if (!empty($product->image_path)) { ?>
              <img class="img" src="<?= base_url() ?>/uploads/<?= $product->image_path; ?>">
              <?php } else { ?>
              <img class="img" src="<?= base_url() ?>/no-image.png">
              <?php } ?>
            </div>
            <div class="col col-12 assets">
              <hr>
              <?php foreach ($product->assets as $asset) { ?>
                <?php if ($asset->type == 'youtube') { ?>
                <iframe width="560" height="315" src="<?= $asset->embed_url?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <?php } else { ?>
                <img class="img" src="<?= base_url() ?>/uploads/<?= $asset->image_path; ?>">
                <?php } ?>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
      <div class="col col-12 col-sm-8">
        <div class="w-100">
          <div class="row">
            <div class="col col-sm-12 quantity">
            </div>
            <div class="col col-sm-12 order">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row" style="margin: 20px;">
    <div class="col col-sm-12">
      <h5><?= $product->name; ?></h5>
    </div>
    <div class="col col-sm-12">
      <hr>
      <p><?= $product->description; ?></p>
    </div>
  </div>
</div>