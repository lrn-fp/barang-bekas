<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Barang Bekas</title>
  <meta name="description" content="The small framework with powerful features">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" type="image/png" href="/favicon.ico"/>

  <link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
  <style>
    body {background-color: #f8f9fd;}
  </style>

</head>

<body>

  <section class="ftco-section">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-7 col-lg-5" style="margin-top: 50px;">
          <div class="card">
            <img class="card-img-top" src="https://preview.colorlib.com/theme/bootstrap/login-form-15/images/xbg-1.jpg.pagespeed.ic.EtoN2PmO7Y.webp">
            <div class="login-wrap p-4 p-md-5">
              <div class="d-flex">
                <div class="w-100">
                  <h3 class="mb-4">Login</h3>
                </div>
              </div>
              <form action="#" action="<?= base_url(); ?>/admin/login" method="post">
                <div class="form-group mt-3">
                  <label for="passwordInput" class="form-label">Email</label>
                  <input type="email" class="form-control" name="email" id="emailInput" required>
                </div>
                <div class="form-group">
                  <label for="passwordInput" class="form-label">Password</label>
                  <input type="password" class="form-control" name="password" id="passwordInput" required>
                </div>
                <div class="form-group">
                  <label for="submit" class="form-label"></label>
                  <button type="submit" class="form-control btn btn-primary rounded submit px-3">Login</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>