<div class="row" style="margin:20px 0px;">
  <div class="col">
  <?php foreach ($categories as $category) { ?>
    <a href="<?= base_url() ?>/category/<?= urlencode($category->name); ?>">
      <button class="btn btn-outline-primary btn-block me-4">
        <?= $category->name; ?>
      </button>
    </a>
  <?php } ?>
  </div>
</div>
<div class="row">

  <?php foreach ($products as $product) { ?>
  <div class="col col-sm-4 col-xl-3">
    <div class="card">
      <a href="<?= base_url() ?>/product/<?= $product->id; ?>-<?= urlencode($product->name); ?>">
        <?php if (!empty($product->image_path)) { ?>
        <img class="card-img-top" src="<?= base_url() ?>/uploads/<?= $product->image_path; ?>">
        <?php } else { ?>
        <img class="card-img-top" src="<?= base_url() ?>/no-image.png">
        <?php } ?>
      </a>
      <div class="login-wrap p-4 p-md-5">
        <a href="<?= base_url() ?>/product/<?= $product->id; ?>-<?= urlencode($product->name); ?>">
          <h5><?= $product->name; ?></h5>
        </a>
      </div>
  </div>
  </div>
  <?php } ?>

</div>