<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Barang Bekas</title>
  <meta name="description" content="The small framework with powerful features">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" type="image/png" href="<?= base_url(); ?>/b.png"/>

  <link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
</head>

<body>

  <header class="p-3 text-bg-dark">
    <div class="container">
      <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
        <a href="/" class="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
        <img src="<?= base_url(); ?>/b.png" width="40" height="32"/>
        </a>

        <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
          <li><a href="<?= base_url(); ?>/" class="nav-link px-2 text-secondary">Home</a></li>
        </ul>

        <form class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3" role="search">
          <input type="search" class="form-control form-control-dark text-bg-dark" placeholder="Search..." aria-label="Search">
        </form>

        <?php if (empty($user)) { ?>
        <div class="text-end">
          <button type="button" class="btn btn-outline-light me-2" data-bs-toggle="modal" data-bs-target="#loginModal">Login</button>
          <a href="<?= base_url(); ?>/signup"><button type="button" class="btn btn-warning">Sign Up</button></a>
        </div>
        <div class="modal fade" id="loginModal" tabindex="-1" aria-labelledby="loginModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content" style="color:black;">
              <div class="modal-header">
                <h5 class="modal-title black" id="loginModalLabel">Login</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <form class="row g-3" action="<?= base_url(); ?>/login" method="post">
                  <div class="mb-3">
                    <label for="emailInput" class="form-label">Email address</label>
                    <input type="email" class="form-control" id="emailInput" name="email" placeholder="name@example.com" required>
                  </div>
                  <div class="mb-3">
                    <label for="passwordInput" class="form-label">Password</label>
                    <input type="password" class="form-control" name="password" id="passwordInput" rows="3" required>
                  </div>
                  <div class="mb-3">
                    <button type="submit" class="btn btn-outline-success">Login</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <?php } else { ?>
        <div class="text-end">
          <a href="<?= base_url(); ?>/cart"><button type="button" class="btn btn-info me-2"><i class="ri-shopping-cart-line"></i> <?= $total_cart; ?></button></a>
        </div>
        <div class="dropdown text-end">
          <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
            <img src="https://github.com/mdo.png" alt="mdo" width="32" height="32" class="rounded-circle">
          </a>
          <ul class="dropdown-menu text-small">
            <li><a class="dropdown-item" href="<?= base_url(); ?>/logout">Logout</a></li>
          </ul>
        </div>
        <?php } ?>
      </div>
    </div>
  </header>

  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-10">
        <?php if (session()->getFlashdata('login-failed')) { ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
          <div>
            <?= session()->getFlashdata('login-failed'); ?>
          </div>
          <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        <?php } ?>
