<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse" style="">
  <div class="position-sticky pt-3 sidebar-sticky">
    <ul class="nav flex-column">
      <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="#">
          <i class="ri-home-4-line"></i>
          Dashboard
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">
          <i class="ri-file-list-3-line"></i>
          Orders <span class="count"><?= $incoming_order_count ?? 0; ?></span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">
          <i class="ri-archive-line"></i>
          Products
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">
          <i class="ri-booklet-line"></i>
          Category
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">
          <i class="ri-numbers-line"></i>
          Reports
        </a>
      </li>
    </ul>
  </div>
</nav>