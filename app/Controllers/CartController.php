<?php

namespace App\Controllers;

class CartController extends BaseController
{
    public function index()
    {
        $user = $this->session->get('user') ?? null;
        if (empty($user) || !in_array('Customer', $user['roles'])) {
            return redirect()->to('/');
        }
        $data = array();
        $data['user'] = $user;
        $data['total_cart'] = $this->session->get('total_cart') ?? 0;
        $data['page'] = 'Cart';
        $data['carts'] = array();

        return view('components/header', $data)
            . view('components/sidebar', $data)
            . view('pages/cart', $data)
            . view('components/footer');
    }
}
