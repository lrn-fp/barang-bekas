<?php

namespace App\Controllers;

use App\Models\CategoryModel;
use App\Models\ProductModel;

class HomeController extends BaseController
{
    public function index()
    {
        $data = array();
        $user = $this->session->get('user') ?? null;
        if (!empty($user) && in_array('Customer', $user['roles'])) {
            $data['user'] = $user;
            $data['total_cart'] = $this->session->get('total_cart') ?? 0;
        }

        $categoryModel = new CategoryModel();
        $data['categories'] = $categoryModel->where('parent_id', null)->findAll();

        $productModel = new ProductModel();
        $data['products'] = $productModel->where('quantity >', 0)->limit(20)->orderBy('updated_at', 'DESC')->findAll();

        return view('components/header', $data)
            . view('pages/home', $data)
            . view('components/footer');
    }
}
