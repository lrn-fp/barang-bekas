<?php

namespace App\Controllers;

class ShipmentController extends BaseController
{
    public function index()
    {
        $user = $this->session->get('user') ?? null;
        if (empty($user) || !in_array('Customer', $user['roles'])) {
            return redirect()->to('/');
        }
        $data = array();
        $data['user'] = $user;
        $data['page'] = 'Order';
        $data['total_cart'] = $this->session->get('total_cart') ?? 0;
        $data['shipments'] = array();

        return view('components/header', $data)
            . view('components/sidebar', $data)
            . view('pages/shipment', $data)
            . view('components/footer');
    }
}
