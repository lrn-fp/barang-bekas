<?php

namespace App\Controllers;

use App\Models\UserModel;
use App\Models\RoleModel;
use App\Models\UserRoleModel;
use App\Models\CartModel;

class AuthController extends BaseController
{
    public function login()
    {
        $user = $this->session->get('user') ?? null;
        if (!empty($user) && in_array('Customer', $user['roles'])) {
            return redirect()->to('/');
        }

        if (! $this->validate([
            'email' => "required",
            'password' => 'required',
        ])) {
            $this->session->setFlashdata('login-failed', 'Login failed, email and password required');
            return redirect()->to('/');
        }

        $email = $this->request->getVar('email');
        $password = $this->request->getVar('password');
        
        $userModel = new UserModel();
        $roleModel = new RoleModel();
        $userRoleModel = new UserRoleModel();

        $dbUser = $userModel->where('email', $email)->first();
        if (!empty($dbUser)) {
            if (!password_verify($password, $dbUser->password)) {
                $this->session->setFlashdata('login-failed', 'Login failed, wrong password');
                return redirect()->to('/');
            }
            $userRoles = $userRoleModel->where('user_id', $dbUser->id)->findAll();
            $isCustomer = false;
            $roles = [];
            foreach ($userRoles as $userRole) {
                $customerRole = $roleModel->where('id', $userRole->role_id)->first();
                if ($customerRole->name == 'Customer') {
                    $isCustomer = true;
                }
                $roles[] = $customerRole->name;
            }
            if ($isCustomer) {
                $dataUser = [
                    'id' => $dbUser->id,
                    'email' => $dbUser->email,
                    'name' => $dbUser->name,
                    'roles' => $roles
                ];
                $this->session->set('user', $dataUser);
                $cartModel = new CartModel();
                $cartCount = $cartModel->where('user_id', $dbUser->id)->where('state', 1)->countAllResults();
                $this->session->set('total_cart', $cartCount);
                return redirect()->to('/');
            }
        }
        
        $this->session->setFlashdata('login-failed', 'Login failed, email not found');
        return redirect()->to('/');
    }

    public function logout()
    {
        $this->session->remove('user');
        $this->session->remove('total_cart');
        return redirect()->to('/');
    }

    public function signupPage()
    {
        $user = $this->session->get('user') ?? null;
        if (!empty($user) && in_array('Customer', $user['roles'])) {
            return redirect()->to('/');
        }

        return view('pages/signup');
    }

    public function signup()
    {
        $user = $this->session->get('user') ?? null;
        if (!empty($user) && in_array('Customer', $user['roles'])) {
            return redirect()->to('/');
        }
        
        if (! $this->validate([
            'email' => "required",
            'password' => 'required',
        ])) {
            $this->session->setFlashdata('signup-failed', 'Email and password required');
            return redirect()->to('/');
        }

        $email = $this->request->getVar('email');
        $password = $this->request->getVar('password');

        if (!empty($user) && $user['email'] != $email) {
            $this->session->setFlashdata('signup-failed', 'You are already logged in with another account');
            return redirect()->to('/');
        }

        $userModel = new UserModel();
        $roleModel = new RoleModel();
        $userRoleModel = new UserRoleModel();

        $customerRole = $roleModel->where('name', 'Customer')->first();

        if (empty($user)) {
            $dbUser = $userModel->where('email', $email)->first();
            if (empty($dbUser)) {
                $userModel->where('email', $email)->first();
                $dataUser = [
                    'email' => $email,
                    'password' => password_hash($password, PASSWORD_BCRYPT),
                ];
                $userModel->insert($dataUser);
                $dataUserRole = [
                    'role_id' => $customerRole->id,
                    'user_id' => $userModel->getInsertID(),
                ];
                $userRoleModel->insert($dataUserRole);
            } else {
                if (password_verify($password, $dbUser->password)) {
                    $dataUserRole = [
                        'role_id' => $customerRole->id,
                        'user_id' => $dbUser->id(),
                    ];
                    $userRoleModel->insert($dataUserRole);
                } else {
                    $this->session->setFlashdata('signup-failed', 'Incorrect password');
                    return redirect()->to('/signup');
                }
            }
        } else {
            $userModel->where('email', $email)->first();
            $dataUser = [
                'email' => $email,
                'password' => password_hash($password, PASSWORD_BCRYPT),
            ];
            $userModel->insert($dataUser);
            $dataUserRole = [
                'role_id' => $customerRole->id,
                'user_id' => $userModel->getInsertID(),
            ];
            $userRoleModel->insert($dataUserRole);
        }

        $this->session->setFlashdata('signup-success', 'SignUp success, please login to continue');
        return redirect()->to('/signup');
    }
}
