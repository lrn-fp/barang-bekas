<?php

namespace App\Controllers;

use App\Models\ProductModel;

class ProductController extends BaseController
{
    public function createProduct()
    {
        return view('welcome_message');
    }

    public function updateProduct()
    {
        $user = $this->session->get('user') ?? null;
        if (empty($user) || !in_array('Admin', $user['roles'])) {
            return redirect()->to('/admin/login');
        }
        return redirect()->to('/admin/product/'.$productId);
    }

    public function createProductPage()
    {
        $user = $this->session->get('user') ?? null;
        if (empty($user) || !in_array('Admin', $user['roles'])) {
            return redirect()->to('/admin/login');
        }
        return view('pages/admin/product-create');
    }

    public function detailProductPage($seg1 = null)
    {
        if (empty($seg1)) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        $data = array();
        $user = $this->session->get('user') ?? null;
        if (!empty($user) && in_array('Customer', $user['roles'])) {
            $data['user'] = null;
            $data['total_cart'] = $this->session->get('total_cart') ?? 0;
        }

        $productId = explode("-", $seg1)[0];

        $productModel = new ProductModel();
        $product = $productModel->where('id', $productId)->where('quantity >', 0)->first();

        if (empty($product)) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }
        $product->assets = [];
        $data['product'] = $product;

        return view('components/header', $data)
            . view('pages/product-detail', $data)
            . view('components/footer');
    }

    public function searchProductPage()
    {
        $data = array();
        $limit = 10;
        $page = 1;
        $offset = $limit*$page;
        $name = '%%';

        $data = array();
        $user = $this->session->get('user') ?? null;
        if (!empty($user) && in_array('Customer', $user['roles'])) {
            $data['user'] = $user;
            $data['total_cart'] = $this->session->get('total_cart') ?? 0;
        }

        $productModel = new ProductModel();
        $data['products'] = $productModel->where('name like', $name)->where('quantity', '>', 0)->limit($limit, $offset)->orderBy('updated_at', 'DESC')->findAll();
        $data['total'] = $productModel->where('name like', $name)->where('quantity', '>', 0)->countAll();

        $data['products'] = array();
        $data['filter'] = array();

        return view('components/header', $data)
            . view('components/filter', $data)
            . view('pages/search', $data)
            . view('components/footer');
    }

    public function detailProductAdminPage()
    {
        $user = $this->session->get('user') ?? null;
        if (empty($user) || !in_array('Admin', $user['roles'])) {
            return redirect()->to('/admin/login');
        }
        return view('pages/admin/product-detail');
    }
}
