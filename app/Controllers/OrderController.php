<?php

namespace App\Controllers;

class OrderController extends BaseController
{
    public function index()
    {
        $user = $this->session->get('user') ?? null;
        if (empty($user) || !in_array('Customer', $user['roles'])) {
            return redirect()->to('/');
        }
        $data = array();
        $data['user'] = $user;
        $data['cart_total'] = 0;
        $data['page'] = 'Order';
        $data['orders'] = array();

        return view('components/header', $data)
            . view('components/sidebar', $data)
            . view('pages/order', $data)
            . view('components/footer');
    }
}
