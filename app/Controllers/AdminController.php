<?php

namespace App\Controllers;

use App\Models\UserModel;
use App\Models\RoleModel;
use App\Models\UserRoleModel;

class AdminController extends BaseController
{
    public function login()
    {
        $user = $this->session->get('user') ?? null;
        if (!empty($user) && in_array('Admin', $this->session->get('roles') ?? [])) {
            return redirect()->to('/admin');
        }
        if (! $this->validate([
            'email' => "required",
            'password' => 'required',
        ])) {
            $this->session->setFlashdata('login-failed', 'Login failed, email and password required');
            return redirect()->to('/');
        }

        $email = $this->request->getVar('email');
        $password = $this->request->getVar('password');
        
        $userModel = new UserModel();
        $roleModel = new RoleModel();
        $userRoleModel = new UserRoleModel();

        $dbUser = $userModel->where('email', $email)->where('password', password_hash($password, PASSWORD_BCRYPT))->first();
        if (!empty($dbUser)) {
            
            $userRoles = $userRoleModel->where('user_id', $userRole->id)->findAll();
            $isAdmin = false;
            $roles = array();
            foreach ($userRoles as $userRole) {
                $customerRole = $roleModel->where('id', $userRole->role_id)->first();
                if ($customerRole->name == 'Admin') {
                    $isAdmin = true;
                }
                $roles = array_push($customerRole, $customerRole->name);
            }
            if ($isAdmin) {
                $dataUser = [
                    'id' => $dbUser->id,
                    'email' => $dbUser->email,
                    'name' => $dbUser->name,
                ];
                $this->session->set('roles', $roles);
                $this->session->set('user', $dataUser);
                return redirect()->to('/admin');
            }
        }
        
        $this->session->setFlashdata('login-failed', 'Login failed, email not found or wrong password');
        return redirect()->to('/admin/login');
    }

    public function loginPage()
    {
        $user = $this->session->get('user') ?? null;
        if (!empty($user) && in_array('Admin', $this->session->get('roles') ?? [])) {
            return redirect()->to('/admin');
        }

        return view('pages/admin/login');
    }

    public function logout()
    {
        $this->session->remove('user');
        $this->session->remove('total_cart');
        return redirect()->to('/admin');
    }

    public function dashboard()
    {
        // $user = $this->session->get('user') ?? null;
        // if (empty($user) || !in_array('Admin', $user['roles'])) {
        //     return redirect()->to('/admin/login');
        // }

        $data = array();
        $data['incoming_order_count'] = 0;

        return view('components/admin/header', $data)
            . view('components/admin/sidebar', $data)
            . view('pages/admin/home', $data)
            . view('components/admin/footer', $data);
    }
}
